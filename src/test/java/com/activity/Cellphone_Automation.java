package com.activity;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.Select;

public class Cellphone_Automation {

	public static void main(String[] args) throws InterruptedException {
		System.setProperty("webdriver.chrome.driver", "/home/ajay/chromedriver_linux64/chromedriver");
		WebDriver driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

		driver.findElement(By.xpath("//a[contains(text(),'Log in')]")).click();
		driver.findElement(By.id("Email")).sendKeys("ajaychandru12@gmail.com");
		driver.findElement(By.id("Password")).sendKeys("Ajay@1999");
		driver.findElement(By.xpath("//input[@value='Log in']")).click();

		WebElement cellphone = driver.findElement(By.xpath("//a[contains(text(),'Electronics')][1]"));
		Actions act = new Actions(driver);
		act.moveToElement(cellphone).build().perform();
		act.moveToElement(driver.findElement(By.xpath("//a[contains(text(),'Cell phones')][1]"))).click().perform();
		driver.findElement(By.id("products-orderby")).click();
	    Thread.sleep(2000);
	    WebElement element =driver.findElement(By.id("products-orderby"));
	    Select sel = new Select(element);
	    sel.selectByValue("https://demowebshop.tricentis.com/cell-phones?orderby=10");
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//input[@value='Add to cart'][1]")).click();
	    Thread.sleep(3000);
	    driver.findElement(By.id("add-to-cart-button-80")).click();
	    driver.findElement(By.linkText("Shopping cart")).click();
	    driver.findElement(By.id("termsofservice")).click();
	    driver.findElement(By.id("checkout")).click();
	    driver.findElement(By.xpath("//div[@id='billing-buttons-container']//input[@value='Continue']")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.id("PickUpInStore")).click();
	    driver.findElement(By.xpath("(//input[@value='Continue'])[2]")).click();
	    Thread.sleep(5000);
	    driver.findElement(By.xpath("(//input[@value='Continue'])[4]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("(//input[@value='Continue'])[5]")).click();
	    Thread.sleep(2000);
	    driver.findElement(By.xpath("//input[@value='Confirm']")).click();
	    Thread.sleep(2000);
	    WebElement thank = driver.findElement(By.xpath("//h1[contains(text(),'Thank you')]"));
	    String thanks = thank.getText();
	    System.out.println(thanks);
	    WebElement order = driver.findElement(By.xpath("//strong[contains(text(),'Your order has been successfully processed!')]"));
	    String orders=order.getText();
	    System.out.println(orders);
	    Thread.sleep(2000);
	    WebElement id = driver.findElement(By.xpath("//ul[@class='details']"));
	    String ids = id.getText();
	    System.out.println(ids);
	   
	    driver.findElement(By.xpath("//input[@value='Continue']")).click();
	    driver.quit();

	}

}
